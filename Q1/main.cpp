#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <math.h>
#include <sys/time.h>
#include "omp.h"

#define SIZE 1048576
#define NUMBER_OF_THREADS 4

using namespace std;

float* random_generator() { 
	float* arr = new float[SIZE];
	for(int i=0; i<SIZE; i++)
		arr[i] = float(rand())/float((RAND_MAX)) * 1000000;
	return arr; 
}

long serial_run(float * random_numbers, bool print) {
	struct timeval start, end;
		
	gettimeofday(&start, NULL);

	float max = random_numbers[0];
	long index = 0;

	for (long j=1; j<SIZE; j++) {
		if (random_numbers[j] > max) {
			max = random_numbers[j];
			index = j;
		}
	}

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nSerial:\n");
		printf("Maximum: %f , Index: %ld\n", max, index);
		printf("Time: %ld microsecond\n", micros);
	}

	return micros;
}

long parallel_run(float* random_numbers, bool print) {
	struct timeval start, end;
	
	gettimeofday(&start, NULL);

	float max = random_numbers[0];
	float localMax;
	long index = 0;
	long localIndex;
	int j;
	
	#pragma omp parallel num_threads(NUMBER_OF_THREADS) shared(random_numbers, max, index) private(localMax, localIndex)
	{
		localMax = random_numbers[0];
		localIndex = 0;
		
		#pragma omp for private(j)
			for (long j=1; j<SIZE; j++) {
				if (random_numbers[j] > localMax) {
					localMax = random_numbers[j];
					localIndex = j;
				}
			}

		#pragma omp critical(final_max)
		{
			if (localMax > max) {
				max = localMax;
				index = localIndex;
			}
		}
	}

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nParallel:\n");
		printf("Maximum: %f , Index: %ld\n", max, index);
		printf("Time: %ld microsecond\n", micros);
	}

	return micros;
}

float part1() {
	float* r = random_generator();
	long sTime = serial_run(r, false);
	long pTime = parallel_run(r, false);
	return float(sTime)/float(pTime);
}


int main() { 
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n");

	printf("**************************************\n");

	printf("Part 1:\n");
	float speed_up_part1 = 0;
	for (int i=0; i<20; i++)
		speed_up_part1 = speed_up_part1 + part1();
	printf("\nAverage speed Up of Part 1: %f\n", speed_up_part1/20);
	
	return 0; 
}
