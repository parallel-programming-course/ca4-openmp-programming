#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <math.h>
#include <sys/time.h>
#include "omp.h"

#define SIZE 1048576

using namespace std;


float* random_generator() { 
	float* arr = new float[SIZE];   
	for(int i=0; i<SIZE; i++)
		arr[i] = float(rand())/float((RAND_MAX)) * 1000000;
	return arr; 
} 

float* copy_float_array(float *arr) { 
	float* copy = new float[SIZE];   
	for(int i=0; i<SIZE; i++)
		copy[i] = arr[i] ;
	return copy; 
} 

int partition(float *random_number, int start, int end) {
	int p = random_number[end];
	int i = start-1;
	for (int j=start; j<end; j++) {
		if (random_number[j] < p) {
			i = i+1;
			float temp = random_number[i];
			random_number[i] = random_number[j];
			random_number[j] = temp;
		}
	}
	random_number[end] = random_number[i+1];
	random_number[i+1] = p;
	return (i+1);
}


void quick_sort(float *random_number, int start, int end) {
	if (start < end) {
		int p = partition(random_number, start, end);
		quick_sort(random_number, start, p-1);
		quick_sort(random_number, p+1, end);
	}
}


void parallel_quick_sort(float* random_number, int start, int end) {
	if (start >= end)
		return;

	int p = partition(random_number, start, end);

	if ((end-start+1) < 2000) {
		#pragma omp task
			quick_sort(random_number, start, p-1);
		#pragma omp task	
			quick_sort(random_number, p+1, end);
	}
	else {		
		#pragma omp task
			parallel_quick_sort(random_number, start, p-1);
		#pragma omp task	
			parallel_quick_sort(random_number, p+1, end);
	}

}

void separate_array(float* random_number, int start, int end) {

	int p2 = partition(random_number, start, end);
	int p1 = partition(random_number, start, p2-1);
	int p3 = partition(random_number, p2+1, end);

	#pragma omp parallel num_threads(4) default(shared)
	{
		#pragma omp single nowait
		{
			#pragma omp task
				parallel_quick_sort(random_number, start, p1-1);
			#pragma omp task
				parallel_quick_sort(random_number, p1+1, p2-1);
			#pragma omp task
				parallel_quick_sort(random_number, p2+1, p3-1);
			#pragma omp task
				parallel_quick_sort(random_number, p3+1, end);
		}
	}
}


long part2_parallel(float* random_number) {
	struct timeval start, end;
	gettimeofday(&start, NULL);
	separate_array(random_number, 0, SIZE-1);
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);
	return micros;
}


long part2_serial(float* random_number) {
	struct timeval start, end;
	gettimeofday(&start, NULL);
	quick_sort(random_number, 0, SIZE-1);
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);
	return micros;
}


float part2() {
	float* r = random_generator();
	float* rs = copy_float_array(r);
	float* rp = copy_float_array(r);
	long sTime = part2_serial(rs);
	long pTime = part2_parallel(rp);
    return float(sTime)/float(pTime);
}


int main() { 
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n");

	printf("**************************************\n");

	printf("Part 2:\n");
	float speed_up_part2 = 0;
	for (int i=0; i<10; i++)
		speed_up_part2 = speed_up_part2 + part2();
	printf("\nAverage speed Up of Part 2: %f\n", speed_up_part2/10);
	
	return 0; 
}